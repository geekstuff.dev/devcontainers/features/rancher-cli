# Geekstuff.dev / Devcontainers / Features / Rancher-CLI

This devcontainer feature installs Rancher CLI.

## How to use

In your `.devcontainer/devcontainer.json`, add this feature elements:

```json
{
    "name": "my devcontainer",
    "image": "debian:bullseye",
    "features": {
        "ghcr.io/geekstuff-dev/devcontainers-features/rancher-cli": {}
    }
}
```

You can use a `debian`, `ubuntu` or `alpine` image as the base.

Full list of source tags are [available here](https://gitlab.com/geekstuff.dev/devcontainers/features/rancher-cli/-/tags).
